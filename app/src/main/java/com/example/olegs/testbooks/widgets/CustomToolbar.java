package com.example.olegs.testbooks.widgets;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.olegs.testbooks.R;
import com.example.olegs.testbooks.tools.AnimUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CustomToolbar extends Toolbar implements View.OnClickListener{

    @Bind(R.id.tvTitle)
    protected TextView tvTitle;
    @Bind(R.id.iBtnBack)
    protected ImageView iBtnBack;

    public CustomToolbar(Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_toolbar, this, true);
    }

    public CustomToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_toolbar, this, true);

        ButterKnife.bind(this);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomToolbar);
        try {
            String title = a.getString(R.styleable.CustomToolbar_titleText);
            if (title!=null) tvTitle.setText(title);
            setBtnBack(a.getBoolean(R.styleable.CustomToolbar_backButton, false));
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            a.recycle();
        }
        init();
    }

    private void init() {
        iBtnBack.setOnClickListener(this);
    }

    @Override
    public void setTitle(CharSequence title) {
        if (title!=null) {
            tvTitle.setText(title);
            AnimUtils.setVisibleTV(tvTitle);
        }
    }

    public void setBtnBack(boolean status){
        int visible = status ? View.VISIBLE : View.GONE;
        iBtnBack.setVisibility(visible);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iBtnBack:
                ((Activity)iBtnBack.getContext()).onBackPressed();
                break;
        }
    }
}