package com.example.olegs.testbooks.data;

import android.content.Context;

import com.example.olegs.testbooks.R;
import com.example.olegs.testbooks.model.Book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;



public class InsertBooks {
    private Context context;
    private InputStream is;
    private static InsertBooks sInsertBooks;
    private ArrayList<Book> listData;

    public static InsertBooks get(Context context) {
        if(sInsertBooks==null){
            sInsertBooks= new InsertBooks(context);
        }
        return sInsertBooks;
    }
    private InsertBooks(Context context) {
        this.context = context;
        listData = new ArrayList<>();
        insertList();
    }

    public ArrayList<Book> getBoks(){
        return listData;
    }

    private void insertList() {
        ArrayList<String> sampleList = new ArrayList<>();

        is = context.getResources().openRawResource(R.raw.books);

        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        try {
            while ((line = br.readLine()) != null) {
                sampleList.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Random random = new Random();

        for (int i = 0; i < 20; i++) {
            Book data = new Book();
            data.setBookName(sampleList.get(0)+String.valueOf(random.nextInt(9) + 1));
            String uri = "@drawable/img"+String.valueOf(random.nextInt(5) + 1);
            int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
            data.setBookImage(imageResource);
            data.setBookRaiting(random.nextInt(5) + 1);
            data.setBookDescr(sampleList.get(1));

            listData.add(data);
        }
    }
}

