package com.example.olegs.testbooks;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;


import com.example.olegs.testbooks.widgets.CustomToolbar;

import butterknife.Bind;
import butterknife.ButterKnife;


public abstract class BaseDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    @Bind(R.id.baseDrawerLayout)
    DrawerLayout drawer;
    CustomToolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.activity_base_drawer);

        FrameLayout flContent = (FrameLayout) findViewById(R.id.flContent);
        View.inflate(this, setLayoutRes(), flContent);
        ButterKnife.bind(this);
        toolbar = (CustomToolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setNavigationItemSelectedListener(this);
    }

    protected abstract int setLayoutRes();


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public CustomToolbar getToolbar() {
        return toolbar;
    }

    public void replaceFragment(Fragment fragment, boolean stack){
        FragmentTransaction fragmentTransaction = this
                .getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction
                .replace(R.id.flContent, fragment);
        if (stack) fragmentTransaction.addToBackStack(fragment.getClass().getCanonicalName());
        fragmentTransaction.commit();
    }
}
