package com.example.olegs.testbooks.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.olegs.testbooks.BaseDrawerActivity;
import com.example.olegs.testbooks.MainActivity;
import com.example.olegs.testbooks.R;
import com.example.olegs.testbooks.fragments.BookViewFragment;
import com.example.olegs.testbooks.model.Book;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class BookRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Book> mBook;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public TextView postTitle;
        public ImageView postImage;
        public RatingBar rbBook;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            postTitle = (TextView) mView.findViewById(R.id.twBookName);
            postImage = (ImageView) mView.findViewById(R.id.imgBook);
            rbBook = (RatingBar) mView.findViewById(R.id.rtbBookRating);
        }
    }
    public BookRVAdapter(ArrayList<Book> ordersDataFrom) {
        this.mBook = ordersDataFrom;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_books, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, final int position) {
        final ViewHolder holder = (ViewHolder) h;
        holder.postTitle.setText(mBook.get(position).getBookName());
        holder.rbBook.setRating(mBook.get(position).getBookRaiting());
        Glide
                .with(holder.postImage.getContext())
                .load(mBook.get(position).getBookImage())
                .bitmapTransform(new RoundedCornersTransformation(holder.postImage.getContext(), 30, 0,
                        RoundedCornersTransformation.CornerType.ALL))
                .into(holder.postImage);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookViewFragment bookViewFragment = BookViewFragment.getInstance(mBook,position);
                MainActivity mainActivity = (MainActivity) holder.mView.getContext();
                mainActivity.replaceFragment(bookViewFragment, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBook.size();
    }

}