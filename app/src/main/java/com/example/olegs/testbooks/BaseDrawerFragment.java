package com.example.olegs.testbooks;

import android.support.v4.app.Fragment;


public class BaseDrawerFragment extends Fragment {
    public BaseDrawerActivity getAct(){
        return (BaseDrawerActivity)getActivity();
    }

    public void initToolbar(String title) {
        getAct().getToolbar().setTitle(title);
        getAct().getToolbar().setBtnBack(true);
    }
}
