package com.example.olegs.testbooks.adapters;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

public class SwipeEvents implements View.OnTouchListener {
    private final static String TAG = "SwipeEvents";
    private SwipeCallback swipeCallback;
    private SwipeSingleCallback swipeSingleCallback;
    private SwipeDirection detectSwipeDirection;

    float x1, x2, y1, y2;
    View view;
    int dimensionX, dimensionY;

    public SwipeEvents(View view) {
        this.view = view;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity)view.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        dimensionX=width/12;
        dimensionY=height/15;
    }

    public void detect(SwipeCallback swipeCallback ){
        this.swipeCallback = swipeCallback;
        view.setOnTouchListener(this);
    }

    public void reset(){
        if (view==null) return;
        if (swipeCallback==null) return;
        swipeCallback = null;
        view.setOnTouchListener(null);
    }

    /*public static void detectTop( View view, SwipeSingleCallback swipeSingleCallback){
        SwipeEvents evt = SwipeEvents.newInstance();
        evt.swipeSingleCallback = swipeSingleCallback;
        evt.view = view;
        evt.detectSingle(SwipeDirection.TOP);
    }

    public static void detectRight( View view, SwipeSingleCallback swipeSingleCallback){
        SwipeEvents evt = SwipeEvents.newInstance();
        evt.swipeSingleCallback = swipeSingleCallback;
        evt.view = view;
        evt.detectSingle(SwipeDirection.RIGHT);
    }

    public static void detectBottom( View view, SwipeSingleCallback swipeSingleCallback){
        SwipeEvents evt = SwipeEvents.newInstance();
        evt.swipeSingleCallback = swipeSingleCallback;
        evt.view = view;
        evt.detectSingle(SwipeDirection.BOTTOM);
    }
    public static void detectLeft( View view, SwipeSingleCallback swipeSingleCallback){
        SwipeEvents evt = SwipeEvents.newInstance();
        evt.swipeSingleCallback = swipeSingleCallback;
        evt.view = view;
        evt.detectSingle(SwipeDirection.LEFT);
    }*/

    /*private void detectSingle( SwipeDirection direction ){
        this.detectSwipeDirection = direction;
        view.setOnTouchListener(this);
    }*/

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                y1 = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                y2 = event.getY();

                SwipeDirection direction = SwipeDirection.TOUCH;

                float xDiff = x2-x1;
                float yDiff = y2-y1;

                if( Math.abs(xDiff) > Math.abs(yDiff)*1.4 ){
                    //Log.d(TAG, "x1: " + x1 + " x2:" + x2 + " dimX: " + dimensionX + " difference: "+xDiff);
                    if (x1+dimensionX<x2){
                        direction = SwipeDirection.RIGHT;
                    }
                    if (x2+dimensionX<x1) {
                        direction = SwipeDirection.LEFT;
                    }
                }else {
                    //Log.d(TAG, "y1: " + y1 + " y2:" + y2 + " dimY: " + dimensionY + " difference: "+yDiff);
                    if (y1-y2 > dimensionY) {
                        direction = SwipeDirection.TOP;
                    }

                    if (y2-y1 > dimensionY) {
                        direction = SwipeDirection.BOTTOM;
                    }
                }
/*
                // Only trigger the requested event only if there
                if ( detectSwipeDirection !=null && swipeSingleCallback !=null ){
                    if ( direction == detectSwipeDirection ){
                        swipeSingleCallback.onSwipe();
                    }
                }else{*/

                switch (direction) {
                    case TOP:
                        swipeCallback.onSwipeTop();
                        break;
                    case RIGHT:
                        swipeCallback.onSwipeRight();
                        break;
                    case BOTTOM:
                        swipeCallback.onSwipeBottom();
                        break;
                    case LEFT:
                        swipeCallback.onSwipeLeft();
                        break;
                    case TOUCH:
                        swipeCallback.onSwipeTouch();
                        break;
                    //}
                }
                break;
        }
        return false;
    }

    public enum SwipeDirection{
        TOP, RIGHT, BOTTOM, LEFT, TOUCH
    }

    public interface SwipeCallback{
        public void onSwipeTop();
        public void onSwipeRight();
        public void onSwipeBottom();
        public void onSwipeLeft();
        public void onSwipeTouch();
    }

    public interface SwipeSingleCallback{
        public void onSwipe();
    }
}