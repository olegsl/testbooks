package com.example.olegs.testbooks;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.olegs.testbooks.adapters.BookRVAdapter;
import com.example.olegs.testbooks.data.InsertBooks;
import com.example.olegs.testbooks.model.Book;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.Bind;
import butterknife.OnClick;


public class MainActivity extends BaseDrawerActivity {

    private static final String TAG = "MainActivity";
    private ArrayList<Book> mBookArrayList;
    @Bind(R.id.rvBooks) RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBookArrayList = InsertBooks.get(this).getBoks();
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        mRecyclerView.setAdapter(new BookRVAdapter(mBookArrayList));

    }

    @Override
    protected int setLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        initToolbar();
    }

    private void initToolbar() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            getToolbar().setBtnBack(false);
            getToolbar().setTitle(getString(R.string.app_name));
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @OnClick (R.id.btnSortName)
    protected void sortByName(){
        Collections.sort(mBookArrayList, new BookNameComparator());
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }
    @OnClick (R.id.btnSortRaiting)
    protected void sortByRate(){
        Collections.sort(mBookArrayList, new BookRaitingComparator());
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    public class BookNameComparator implements Comparator<Book> {

        @Override
        public int compare(Book lhs, Book rhs) {
            return lhs.getBookName().compareTo(rhs.getBookName());
        }
    }

    public class BookRaitingComparator implements Comparator<Book> {

        @Override
        public int compare(Book lhs, Book rhs) {
            int a = lhs.getBookRaiting();
            int b = rhs.getBookRaiting();
            return a < b ? 1 : (a == b ? 0 : -1);
        }
    }
}
