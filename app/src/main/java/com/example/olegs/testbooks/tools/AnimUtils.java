package com.example.olegs.testbooks.tools;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;


public class AnimUtils {
    public static void setVisibleTV(TextView tv){
        ScaleAnimation animation = new ScaleAnimation(
                0, 1, 0, 1
                , Animation.RELATIVE_TO_SELF
                , (float)0.5
                , Animation.RELATIVE_TO_SELF
                , (float)0.5);
        animation.setDuration(300);
        tv.setVisibility(View.VISIBLE);
        tv.startAnimation(animation);
    }

    public static void setUnvisibleTV(TextView tv){
        ScaleAnimation animation = new ScaleAnimation(
                1, 0, 1, 0
                , Animation.RELATIVE_TO_SELF
                , (float)0.5
                , Animation.RELATIVE_TO_SELF
                , (float)0.5);
        animation.setDuration(300);
        tv.setVisibility(View.INVISIBLE);
        tv.startAnimation(animation);
    }
}
