package com.example.olegs.testbooks.model;


public class Book {

    private int bookImage;
    private String bookName;
    private int bookRaiting;
    private String bookDescr;

    public int getBookImage() {
        return bookImage;
    }

    public void setBookImage(int bookImage) {
        this.bookImage = bookImage;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int getBookRaiting() {
        return bookRaiting;
    }

    public void setBookRaiting(int bookRaiting) {
        this.bookRaiting = bookRaiting;
    }

    public String getBookDescr() {
        return bookDescr;
    }

    public void setBookDescr(String bookDescr) {
        this.bookDescr = bookDescr;
    }
}
