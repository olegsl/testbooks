package com.example.olegs.testbooks.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.olegs.testbooks.BaseDrawerFragment;
import com.example.olegs.testbooks.R;
import com.example.olegs.testbooks.adapters.SwipeEvents;
import com.example.olegs.testbooks.model.Book;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;




@SuppressLint("ValidFragment")
public class BookViewFragment extends BaseDrawerFragment {

    private final static String          TAG = "BookViewFragment";
    private final static String          ADAPTER_POSSITION ="adapter_possition";
    private int                          position;
    private ArrayList<Book>              bookArrayList;
    @Bind(R.id.twBookNameItem)TextView   bookName;
    @Bind(R.id.tvDescr) TextView         bookDescr;
    @Bind(R.id.imgBookItem)ImageView     bookImage;
    @Bind(R.id.rtbBookRatingItem) RatingBar rbBook;

    @SuppressLint("ValidFragment")
    public BookViewFragment(ArrayList<Book> bookArrayList) {
        this.bookArrayList=bookArrayList;
    }

    public static BookViewFragment getInstance(ArrayList<Book> mBook, int position) {

        BookViewFragment bookViewFragment = new BookViewFragment(mBook);
        Bundle args = new Bundle();
        args.putInt(ADAPTER_POSSITION,position);
        bookViewFragment.setArguments(args);

        return  bookViewFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args!=null) {
            position = args.getInt(ADAPTER_POSSITION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_view_book, container, false);
        ButterKnife.bind(this,rootView);

        Glide
                .with(bookImage.getContext())
                .load(bookArrayList.get(position).getBookImage())
                .bitmapTransform(new RoundedCornersTransformation(bookImage.getContext(),30, 0,
                        RoundedCornersTransformation.CornerType.ALL))
                .into(bookImage);

        bookName.setText(bookArrayList.get(position).getBookName());
        rbBook.setRating(bookArrayList.get(position).getBookRaiting());
        bookDescr.setText(bookArrayList.get(position).getBookDescr());

        SwipeEvents swipeEvents = new SwipeEvents(rootView);
        swipeEvents.detect(new SwipeEvents.SwipeCallback() {
            @Override
            public void onSwipeTop() {
                Log.d(TAG, "Top");
            }

            @Override
            public void onSwipeRight() {
                Log.d(TAG, "Right");
                if(position >0){
                    position--;
                    Log.d(TAG, String.valueOf(position));
                    BookViewFragment bookViewFragment = BookViewFragment.getInstance(bookArrayList,position);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.rlBook,bookViewFragment,null)
                            .commit();
                }else getActivity().onBackPressed();

            }

            @Override
            public void onSwipeBottom() {
                Log.d(TAG, "Bottom");
            }

            @Override
            public void onSwipeLeft() {
                Log.d(TAG, "Left");
                if(position <bookArrayList.size()-1){
                    position++;
                    Log.d(TAG, String.valueOf(position));
                    BookViewFragment bookViewFragment = BookViewFragment.getInstance(bookArrayList,position);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.rlBook,bookViewFragment,null)
                            .commit();
                }
            }

            @Override
            public void onSwipeTouch() {
                Log.d(TAG, "Touch");
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        initToolbar(bookArrayList.get(position).getBookName());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
